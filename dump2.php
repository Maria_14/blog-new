<?php
include './vendor/autoload.php';

use Blog\Library\FakeDump2;

$faker = \Faker\Factory::create('en_IN');
$faker->addProvider(new \Faker\Provider\Lorem($faker));

$file = '/project/app/dump/dump-fake11.sql';
$dirImages = '/project/app/web/upload/';
new FakeDump2($faker, 500000, ['date_d','author_id', 'title', 'description', 'topic_id', 'images'], 'articles', $file, 10000, $dirImages);
new FakeDump2($faker, 50000, ['id_article', 'id_tag'], 'tag_article', $file, 100000, '');
new FakeDump2($faker, 50, ['name'], 'tag', $file, 50, '');
new FakeDump2($faker, 30, ['name'], 'topic', $file, 30, '');
new FakeDump2($faker, 5000, ['name'], 'author', $file, 1000, '');
