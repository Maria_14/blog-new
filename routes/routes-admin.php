<?php
use Illuminate\Routing\Router;

$router->group(['namespace' => 'Blog\Controllers'], function (Router $router) {
    $router->match(['get','post'], 'login', 'AuthController@login')->name('login');
});


$router->group(['middleware' => 'auth', 'namespace' => 'Blog\Controllers'], function (Router $router) {
    $router->get('/', 'AdminController@index');
    $router->get('article', 'ArticleController@index')->name('article');
    $router->match(['get','post'], 'article/create', 'ArticleController@create');
    $router->match(['get','post'], 'article/update/{id}', 'ArticleController@update')->where('id', '\d+');
    $router->match(['get','post'], 'article/delete/{id}', 'ArticleController@delete')->where('id', '\d+');
    $router->get('logout',  'AuthController@logout')->name('logout');

});
