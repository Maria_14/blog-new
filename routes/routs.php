<?php
use Illuminate\Routing\Router;

$router->group(['namespace' => 'Blog\Controllers'], function (Router $router) {
    $router->match(['get','post'], '/', 'MainController@index');
    $router->match(['get','post'], 'getcloud', 'MainController@getcloud');
    $router->match(['get','post'], 'toppost', 'BlogController@toppost');
    $router->get('post/{id}',  'BlogController@post')->where('id', '\d+');

});
