<?php
session_start();
ini_set('memory_limit', '-1');
include '../vendor/autoload.php';


use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Illuminate\Http\Request;
use Illuminate\Routing\Pipeline;
use Illuminate\Routing\Router;
use Blog\Eloquent\Encapsulator;
use Symfony\Component\HttpFoundation\Response;

define('__ROOT__', '../app');
define('__UPLOAD__', __ROOT__.'/web/upload');

Encapsulator::init();

$container = new Container;



$globalMiddleware = [
    \Blog\Middleware\StartSession::class,
];

$routeMiddleware = [
    'auth' => \Blog\Middleware\Authenticate::class,
    'guest' => \Blog\Middleware\RedirectIfAuthenticated::class,
];


$request = Request::capture();

$container->instance('Illuminate\Http\Request', $request);

$events = new Dispatcher($container);

$router = new Router($events, $container);
require_once '../routes/routes-admin.php';

foreach ($routeMiddleware as $key => $middleware) {
    $router->aliasMiddleware($key, $middleware);
}


try {
    $response = (new Pipeline($container))
        ->send($request)
        ->through($globalMiddleware)
        ->then(function ($request) use ($router) {
            return $router->dispatch($request);
        });

} catch (Symfony\Component\HttpKernel\Exception\HttpException $e) {

    $content = view('404');
    $response =  Response::create($content, 400, []);
}

$response->send();