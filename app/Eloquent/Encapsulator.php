<?php

namespace Blog\Eloquent;

use Illuminate\Container\Container;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use PDO;
class Encapsulator
{
    private static $conn;
    private function __construct() {}
    /**
     * Initialize capsule and store reference to connection
     */
    static public function init()
    {
        if (is_null(self::$conn)) {
            $capsule = new Capsule;
            $capsule->addConnection([
                'driver' => 'mysql',
                'host' => 'mysql',
                'database' => 'article',
                'username' => 'admin',
                'password' => 'admin',
                'charset' => 'utf8',
                'collation' => 'utf8_general_ci',
                'prefix' => '',
                'options'  => array(PDO::MYSQL_ATTR_LOCAL_INFILE => true)
            ]);
            $capsule->setEventDispatcher(new Dispatcher(new Container));
            $capsule->setAsGlobal();
            $capsule->bootEloquent();
        }
    }
}