<?php
namespace Blog\models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = "users";
    protected $fillable = array("login",'pass');
    public $timestamps = false;


    public function testPass($pass, $userPass)
    {
        return (md5($pass)) == $userPass;
    }

}