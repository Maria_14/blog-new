<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 12.11.18
 * Time: 12:50
 */

namespace Blog\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as DB;
use Rakit\Validation\Validator;
class Articles extends Model
{

    protected $table = "articles";
    protected $fillable = array('date_d', 'auth_id', 'title', 'description', 'topic_id',  'images');
    public $timestamps = false;


    public function rule()
    {
        return [
            'author_id'    => 'required',
            'title'        => 'required',
            'topic_id'     => 'required',
            'description'  => 'required|min:6'
        ];
    }
    public function loadedElem(array $mas = [])
    {
        $this->author_id = $mas['author_id'];
        $this->date_d = date('Y-m-d');
        $this->title = $mas['title'];
        $this->description = $mas['description'];
        $this->topic_id = $mas['topic_id'];
        return true;
    }

    public function valid(array $mas = [])
    {
        $validator = new Validator;
        $validation = $validator->validate($mas, $this->rule());
        if ($validation->fails()) {
            $errors = $validation->errors()->toArray();
            return $errors;
        } else {
            return 'success';
        }

    }
    public function athor()
    {
        return $this->belongsTo('Blog\Models\Author','author_id');

    }
    public function tags()
    {
        return $this->belongsToMany('Blog\Models\Tags', 'tag_article', 'id_article', 'id_tag');
    }

    public function topic()
    {
        return $this->belongsTo('Blog\Models\Topic','topic_id');
    }

    public function getcloudcount()
    {
        $tags = DB::table('tag')
            ->leftjoin('tag_article', 'tag.id', '=', 'tag_article.id_tag')
            ->leftjoin('articles', 'articles.id', '=', 'tag_article.id_article')
            ->selectRaw(' count(articles.id) AS `count`, GROUP_CONCAT(tag.name) AS `tag`');
        return $cloudTopic = $tags->groupBy('topic_id')->orderBy('count','desc')->limit(1)->get();

    }
    public function getcloudauthor()
    {
        $tags = DB::table('articles')
            ->leftjoin('tag_article', 'articles.id', '=', 'tag_article.id_article')
            ->leftjoin('tag', 'tag.id', '=', 'tag_article.id_tag')
            ->selectRaw(' GROUP_CONCAT(tag.name) AS `tag`')
            ->join('author', 'author.id', '=', 'articles.author_id');
        return  $cloudAuthor = $tags->groupBy('author_id')->orderBy('author.name', 'asc')->limit(1)->get();
    }
    public function getclouddate()
    {
        $tags = DB::table('articles')
            ->leftjoin('tag_article', 'articles.id', '=', 'tag_article.id_article')
            ->leftjoin('tag', 'tag.id', '=', 'tag_article.id_tag')
            ->selectRaw(' GROUP_CONCAT(tag.name) AS `tag`');
        return $cloudDate = $tags->groupBy('articles.date_d')->orderBy('articles.date_d')->limit(1)->get();
    }
}