<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 15.11.18
 * Time: 8:20
 */

namespace Blog\Models;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{

    protected $table = "author";
    protected $fillable = array('name');
    public $timestamps = false;


    public function articles()
    {
        return $this->hasMany('Blog\Models\Articles');
    }
}