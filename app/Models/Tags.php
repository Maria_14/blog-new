<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 13.11.18
 * Time: 20:27
 */

namespace Blog\Models;

use Illuminate\Database\Eloquent\Model;
class Tags extends Model
{
    protected $table = "tag";
    protected $fillable = array("name");
    public $timestamps = false;


}