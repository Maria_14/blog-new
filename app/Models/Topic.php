<?php
namespace Blog\Models;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $table = "topic";
    protected $fillable = array("name");
    public $timestamps = false;

    public function article()
    {
        return $this->hasMany('Blog\Models\Articles');
    }

}
