<?php
namespace Blog\Models;

use Illuminate\Database\Eloquent\Model;

class Tokens extends Model {
    protected $table = "tokens";
    protected $fillable = array("users_id",'user_ip', 'agent', 'token', 'expires');
    public $timestamps = false;
}