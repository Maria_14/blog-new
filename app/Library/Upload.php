<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 12.11.18
 * Time: 13:49
 */

namespace Blog\Library;


class Upload
{

    public $map =[
        'video/mp4' => '.mp4',
        'video/mpeg' => '.mp3',
        'image/gif' => '.gif',
        'image/jpeg' => '.jpg',
        'image/pjpeg'=> '.jpg',
        'image/png' => '.png'
    ];


    public function unlink($path)
    {
        return unlink($path);
    }

    public function upload ($fileName, $file, $path = null)
    {


        if(!$path){
            $path = __UPLOAD__;
        }
        if(!is_dir($path)){

            mkdir(__UPLOAD__, 0777, true);
        }
        $type = $file[$fileName]['type'];
        $tmpPath = $file[$fileName]['tmp_name'];

        $fullName = $this->generateName($fileName, $type, $path);
        if(file_exists($tmpPath)){
            copy($tmpPath, $path.'/'.$fullName);
            return ['path' => $fullName];
        }
    }
    public function generateName($name, $type, $path)
    {

        foreach ($this->map as $key => $u){

            if($key == $type){
                do {
                    $nameNew = md5(microtime() . rand(0, 9999));
                    $file = $path.'/'.$nameNew.'_'.$name.$this->map[$type];
                } while (file_exists($file));

                return $nameNew.'_'.$name.$this->map[$type];
            }
        }
        return ['error' => sprintf("Не верный тип файла %s", $type)];
    }


   public function find($dir, $tosearch) {


        $files = array_diff( scandir( $dir ), Array( ".", ".." ) );

        foreach( $files as $d ) {
            if( !is_dir($dir."/".$d) ) {
                if ($d == $tosearch)
                    return $this->unlink($dir."/".$d);
            } else {
                $res = $this->find($dir."/".$d, $tosearch);
                if ($res)
                    return $res;
            }
        }
        return false;
    }
}