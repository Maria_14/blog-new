<?php 

namespace Blog\Library;
use Blog\Models\User;


class Auth{
	
	public function user(){
		
		if(isset($_SESSION['user'])){
			return User::find($_SESSION['user']);
		}
		return false;
	}
	/**
	* Check if user is signed in
	* 
	* @return
	*/
	public function check(){
		
		return isset($_SESSION['user']);
	}
	/**
	* Attempt to sign the user in by first checking if the user exists
	* Then check if the plain text password supplied matches the hashed password
	* @param string $email
	* @param string $password
	* 
	* @return
	*/
	public function attempt($login, $password){

		$user = User::where('login', $login)->first();

		if(!$user){
			return false;
		}
		
		if($this->testPass($password, $user->pass)){
			$_SESSION['user'] = $user->id;
			return true;
		}
		
		return false;
	}
    /**
     * Test Pass
     *
     * @param string $pass this param not hash md5
     * @param string $userPass this param get in bd
     *
     * @return
     */

	public function testPass($pass, $userPass)
    {
        return md5($pass) == $userPass;

    }
	
	/**
	* Log User Out by deleting session
	*/
	public function logout(){
		unset($_SESSION['user']);
	}
	
}