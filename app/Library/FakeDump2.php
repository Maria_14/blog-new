<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 02.12.18
 * Time: 8:16
 */

namespace Blog\Library;

class FakeDump2
{
    private $fake;
    private $count;
    private $kay;
    private $table;
    private $multiple;
    private $images;
    private $masImg;
    private $file;


    public function __construct($fake, $count, $kay = [], $table, $file, $multiple = null, $dirImages = null)
    {
        $this->fake = $fake;
        $this->count = $count;
        $this->kay = $kay;
        $this->table = $table;
        $this->multiple = $multiple;
        if($dirImages) {
            $this->masImg = scandir($dirImages);
            $this->images = count($this->masImg);
        }
        $this->file = $file;
        $this->generateMas();
    }

    public function generateMas()
    {

        $array = [];
        $mas = [];


        for ($j = 1; $j < $this->count+1; $j++) {
            if($j%$this->multiple == 0) {
                $this->saveStringPackeg($array);
                $array = [];
            }
            $arrayFake = $this->getValue();
            for ($i = 0; $i < count($this->kay); $i++) {
                $mas[] = $arrayFake[$this->kay[$i]];
            }
            array_push($array,  $mas);
            $mas = [];
        }
    }

    public function getValue()
    {
        switch ($this->table){
            case'author':
                $mas = ['name' => $this->fake->name];
                break;
            case'tag_article':
                $mas = [
                    'id_article' => $this->fake->numberBetween('1', $this->count),
                    'id_tag' => $this->fake->numberBetween('1', 50),
                ];
                break;
            case'tag':
                $mas = ['name' => $this->fake->word];
                break;
            case'topic':
                $mas = ['name' => $this->fake->word];
                break;
            case'articles':
                $mas = [
                    'date_d' => $this->getDate(),
                    'author_id' => $this->fake->numberBetween(1, 5000),
                    'title' => $this->fake->sentence(3),
                    'description' =>$this->fake->realText(),
                    'topic_id' => $this->fake->numberBetween(1, 30),
                    'images' => $this->getImages(),
                ];
                break;
        }
        return $mas;
    }
    public function getDate()
    {
        $dateTime = $this->fake->dateTimeBetween($startDate = '-5 years', $endDate = 'now');
        foreach ($dateTime as $val => $kay) {
            if($val == "date") {
                $date = explode(" ", $kay);
                return $date[0];
            }
        }
    }
    public function getImages()
    {
        $random = $this->fake->numberBetween(3, $this->images);
        return $this->masImg[$random];
    }
    public function saveStringPackeg($arrayValue)
    {
        $csv = new SqlQuery();
        $handle = fopen($this->file, "a");
        $value = $csv->getStringInsertPackeg($this->kay, $arrayValue, $this->table);
        fputcsv($handle, explode(";", $value), ";", ' ');
        fclose($handle);
    }
}