var cloud = {
    init: function () {
        var url = "/getcloud";
        var loader  = $(".loader");
        var self= this;
        var defaultTopic = $(".default-topic");
        var defaultAuthor = $(".default-author");
        var defaultDate = $(".default-date");
        var tag;
        this.ajaxCall(url, [], function(msg) {
            self.generateArray(msg.cloudTopic, defaultTopic);
            self.generateArray(msg.cloudAuthor, defaultAuthor);
            self.generateArray(msg.cloudDate, defaultDate);
        },loader);

    },
    ajaxCall:function (ajax_url, ajax_data, successCallback, loader) {
        $.ajax({
            type : "GET",
            url : ajax_url,
            dataType : "json",
            data: ajax_data,
            success : function(msg) {
                if(msg.error) {
                    console.log(msg);
                } else {
                    successCallback(msg);
                }
            },
            error: function(msg) {
                console.log(msg);
            },
            beforeSend: function() {
                loader.show();
            },
            complete: function() {
                loader.hide();
            }
        });

    },
    generatorCloud: function (array, elem) {
        var li = jQuery('<li/>');
        var link = "";
        $.each(array, function (kay, value) {
            link = jQuery('<a/>',{
                href:'#',
                text:value
            });
            link.appendTo(li);
            li.appendTo(elem);
        });

    },
    generateArray: function (array, elem) {
        var self= this;
        $.each(array, function (kay, value) {
            tag = (value.tag).split(',');
            self.generatorCloud(tag, elem);
            tag = [];
        })
    }
};


window.addEventListener("load", function () {
    cloud.init();
});