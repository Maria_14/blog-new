<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 30.11.18
 * Time: 20:23
 */

namespace Blog\Controllers;

use Blog\Models\Author;
use Blog\Models\Articles;
use Blog\Models\TagArticles;
use Blog\Models\Topic;
use Blog\Models\Tags;
use Blog\Library\TopPush;
use Illuminate\Http\Request;
class ArticleController
{
    public function index(Request $request)
    {

        $perPage = 5;
        $columns = ['*'];
        $pageName = 'page';

        $page = isset($request->page) ? $request->page : null;

        $article = Articles::orderBy('id')->paginate($perPage, $columns, $pageName, $page);

        return view('article.index', [
            'article' => $article

        ]);
    }

    public function create(Request $request)
    {

        if(!empty($_FILES['images']['tmp_name'])) {
            $path = upload()->upload('images', $_FILES, null);
            if($path['error']) {
                $error['images'] = $path['error'];

            }
        }

        if($_POST) {
            $model = new Articles();

            if(!empty($path['path'])) {
                $model->images = $path['path'];
            }


            if($model->loadedElem($_POST) && $response = $model->valid($_POST) == 'success' && $model->save()){
                if($_POST['tags']) {
                    $modelTag = new TagArticles();
                    $modelTag->idArticle =  $model->id;
                    $modelTag->add($_POST['tags']);
                   return redirect("article");
                }
            } else {
                foreach ($response as $kay => $value) {
                    $error[$kay] = $value;
                }
            }
        }
        $modelTopic = Topic::all()->toArray();
        $modelTag = Tags::all()->toArray();
        $modelAuthor = Author::all()->toArray();

        return view('article.create', [
            'error' => $error,
            'modelTopic' =>  $modelTopic,
            'modelTag' =>  $modelTag,
            'modelAuthor' =>  $modelAuthor
        ]);
    }
    public function update(Request $request)
    {

        $id = $request->id;
        $modelArticle = Articles::find($id);
        if(!$modelArticle) return abort(404);

        $topicId = $modelArticle->topic_id;


        $data['id'] = $id;
        $data['topic_id'] = $topicId;
        $data['rout'] = 'update';

        $topPush = new TopPush();
        $topPush->connect($data);


        if(!empty($_FILES['images']['tmp_name'])) {

            upload()->find(__UPLOAD__,$modelArticle->toArray()['images']);

            $path = upload()->upload('images', $_FILES, null);

            if($path['error']) {
                $error['images'] = $path['error'];
            } else {
                $modelArticle->images  = $path['path'];
            }
        }

        if($_POST && !$path['error']) {
            if($modelArticle->loadedElem($_POST) && $response = $modelArticle->valid($_POST) == 'success' && $modelArticle->save()){
                $modelArticle->tags()->detach();

                if($_POST['tags']) {
                    $modelTag = new TagArticles();
                    $modelTag->idArticle = $id;


                    $modelTag->add($_POST['tags']);

                }

                return  redirect("article");
            } else {
                foreach ($response as $kay => $value) {
                    $error[$kay] = $value;
                }
            }
        }

        $tags = [];
        foreach ($modelArticle->tags as $kay => $val)
        {
            $tags[] = $val['id'];
        }
        $modelArticle = $modelArticle->toArray();
        $modelTopic = Topic::all()->toArray();
        $modelAuthor = Author::all()->toArray();
        $modelTag = Tags::all()->toArray();



        return view('article.update', [
            'error' => $error,
            'tags'=>$tags,
            'modelArticle'=>$modelArticle,
            'modelTopic' =>  $modelTopic,
            'modelTag' =>  $modelTag,
            'modelAuthor' =>  $modelAuthor
        ]);
    }
    public function delete(Request $request)
    {

        $modelArticle = Articles::find($request->id);
        if(!$modelArticle) return abort(404);

        $modelArticle->tags()->detach();
        upload()->find(__UPLOAD__,$modelArticle->toArray()['images']);

        $modelArticle->delete();
        return  redirect("/");
    }
}