<?php
/**
* 
*/
namespace Blog\Controllers;

use Illuminate\Database\Capsule\Manager as DB;
use Blog\Models\Articles;
use Blog\Library\TopPush;
use Illuminate\Http\Request;
class BlogController
{

    public function post(Request $request)
    {
        $id = $request->id;
        $modelArticle = Articles::find($id);
        if(empty($modelArticle)) return  abort('404');
        $topicId = $modelArticle->topic_id;

        $data['id'] = $id;
        $data['topic_id'] = $topicId;
        $data['rout'] = 'fixcount';

        $topPush = new TopPush();
        $topPush->connect($data);

        return view('blog.index', [
            'modelArticle' => $modelArticle,

        ]);
    }
    public function toppost(Request $request)
    {

        if(empty($request->post())) {
            echo json_encode(['error'=>'Bad Request']);
        }
        $mas = [];
        foreach($request->post() as $kay => $value){
            foreach ($value as $val => $ky) {
                $topArticle =  DB::table('articles')
                    ->select(['id', 'title', 'images'])
                    ->where('id', '=', $ky['article_id'])
                    ->get()->toArray();

                $topArticleArray = (array) $topArticle[0];
                $topArticleArray['view'] = $ky['count_view'];
                array_push($mas, $topArticleArray);

            }
            return json_encode($mas);
        }


    }
}