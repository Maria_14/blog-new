<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 30.11.18
 * Time: 20:23
 */

namespace Blog\Controllers;
use Illuminate\Http\Request;
use Rakit\Validation\Validator;
use Blog\Models\User;


class AuthController
{
    public function login(Request $request)
    {
        $userModel = new User();

        $password = "";
        $username ="";

        if($request->username && $request->password) {
            $validator = new Validator;

            $validation = $validator->validate($_POST, [
                'username' => 'required',
                'password' => 'required',
            ]);
            $validation->setAliases([
                'username' => 'Error username',
                'password' => 'Error in your password'
            ]);

            if(!empty($validation->errors()->get('username'))) {
                $username = $validation->getAlias('username');
            }

            if(!empty($validation->errors()->get('password'))) {
                $password = $validation->getAlias('password');
            }

            $auth = auth()->attempt($request->username, $request->password);

            if($validation->errors()->count() < 1 && $auth) {

              return redirect("/");
            }
        }
        return view('auth.login', [
            'errorPass' => $password,
            'errorUserName' =>  $username

        ]);
    }
    public function logout()
    {
        auth()->logout();
        return redirect('login');
    }

}