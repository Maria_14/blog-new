<?php
/**
* 
*/
namespace Blog\Controllers;

use Blog\Models\Articles;
use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
class MainController
{
	
	public function index(Request $request)
	{

        $perPage = 30;
        $columns = ['*'];
        $pageName = 'page';

        $page = isset($request->page) ? $request->page : null;

        $article = Articles::orderBy('id')->paginate($perPage, $columns, $pageName, $page);


        return view('main.index', [
            'article' => $article,

        ]);
	}
	public function getcloud()
    {
        $modelArticles = new Articles();
        $mas = [
            'cloudTopic' => $modelArticles->getcloudcount(),
            'cloudAuthor' => $modelArticles->getcloudauthor(),
            'cloudDate' => $modelArticles->getclouddate(),
        ];
        return json_encode($mas);
    }
}