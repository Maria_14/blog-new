@extends('layouts.login')
@section('title', 'Login Page')
@section('content')
<form id="slick-login"  method="POST">
    <div class="helper">{{ $errorUserName  }}</div>
    <input type="text" name="username" class="placeholder" placeholder="admin@example.com"
           value="{{ $_POST["username"] = $_POST["username"] ? $_POST["username"] : ''   }}">
    <div class="helper">{{ $errorPass}}</div>
     <input type="password" name="password" class="placeholder"
            value="{{ $_POST["password"] = $_POST["password"] ? $_POST["password"] : ''   }}"
            placeholder="Сложный пароль...">
    <input type="submit" value="ВОЙТИ">
</form>
@endsection