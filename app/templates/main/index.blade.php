@extends('layouts.layout')
@section('title', 'Blog Page')
@push('scripts')
    <script src="/12345/cloud.js"></script>
@endpush

@section('content')
    <div id="banner"></div>
    <div id="main">
        <div class="container">
            <div class="row">
                <div class="left">
                    <section>
                        <header>
                            <h2>Статьи</h2>
                        </header>
                        <ul type="1" >
                            @foreach ($article as $kay => $val)

                            <li><a class="link" href="/post/{{ $val->id }}">{{ $val->title }}</a></li>
                            @endforeach
                        </ul>
                        <div class="pagination">
                            {{$article->appends($_GET)->links('pagination') }}
                        </div>
                    </section>
                </div>
                <div class="right">
                    <section class="sidebar">
                        <header>
                            <h2>Облако тегов по темам статей</h2>
                            <img class = "loader" src="https://66.media.tumblr.com/a6177f6b977637597850b273022c81ed/tumblr_nurhzkuKQO1syz1nro1_500.gif" >
                        </header>
                        <ul class="default-topic default"></ul>
                    </section>
                    <section class="sidebar">
                        <header>
                            <h2>Облако тегов по авторам</h2>
                            <img class = "loader" src="https://66.media.tumblr.com/a6177f6b977637597850b273022c81ed/tumblr_nurhzkuKQO1syz1nro1_500.gif" >
                        </header>
                        <ul class="default-author default"></ul>
                    </section>
                    <section class="sidebar">
                        <header>
                            <h2>Облако тегов по дате публикации</h2>
                            <img class = "loader" src="https://66.media.tumblr.com/a6177f6b977637597850b273022c81ed/tumblr_nurhzkuKQO1syz1nro1_500.gif" >
                        </header>
                        <ul class="default-date default"></ul>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection