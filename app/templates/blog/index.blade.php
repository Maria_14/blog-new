@extends('layouts.layout')
@section('title', 'Main Page')
@push('scripts')
<script src="/12345/main.js"></script>
@endpush

@section('content')

<div  class="container container-post">
    <div class="header-post">
        <div><a class="author">{{ $auth }}</a></div>
        <div><a class="topic">{{ $topic }}</a></div>
        <div><a class="topic view">Просмотры: <span>{{ $modelArticle['view'] }}</span></a></div>
        <div class="date"> {{ date("d.m.Y", strtotime($modelArticle['date_d'])) }}</div>
    </div>
    <div class="article">
        <h1>{{ $modelArticle['title'] }}</h1>
        <div class="content">
            <img src="/{{$modelArticle['images']}}">{{$modelArticle['description']}}
        </div>
    </div>
    <input type = "hidden" id="article-id" value=" {{ $modelArticle['id'] }}" >
</div>
<div class="container top-article"></div>
@endsection