@extends('layouts.admin')
@section('title', 'Admin Main')
@section('content')
<div class="main">
    <div class="hipsum">
        <div class="jumbotron">
            <h1 id="hello,-world!">Последние добавленные статьи
                <a class="anchorjs-link" href="#hello,-world!">
                    <span class="anchorjs-icon"></span>
                </a>
            </h1>
           <p><a class="links" href="/article/create" role="button">Добавить статью</a></p>
        </div>
    </div>
</div>
@endsection