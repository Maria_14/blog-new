<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>
    <link rel="icon" type="image/png" href="/asset/img/favicon.png">
    <link href="/12345/style.css" rel="stylesheet">
    <link href="/12345/stylefis.css" rel="stylesheet">
</head>

<body style = "background-color: #323B55;">
<div class="wrapper">

    <main role="main" class="container">
        <div class="containerMain">
            @yield('content')
        </div>
    </main>
    <script src="/12345/jquery-3.1.1.js"></script>
    <script src="/12345/main.js"></script>
</div>
</body>
</html>
