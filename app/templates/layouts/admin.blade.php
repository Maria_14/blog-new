<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>  @yield('title')</title>
    <link rel="icon" type="image/png" href="/asset/img/favicon.png">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
    <link href="/12345/admin.css" rel="stylesheet">
</head>

<body>
<div class="wrapper">
    <div class="header">
        <a href="#" id="menu-action">
            <i class="fa fa-bars"></i>
            <span>Закрыть</span>
        </a>
        <div class="logo">
           Админка
        </div>
    </div>
    <div class="sidebar">
        <ul>
            <li><a href="/"><i class="fa fa-server"></i><span>Главная</span></a></li>
            <li><a href="/article/create"><i class="fa fa-plus"></i><span>Добавить статью</span></a></li>
            <li><a href="/article"><i class="fa fa-newspaper-o"></i><span>Все статьи</span></a></li>
            <li><a href="/logout"><i class="fa fa-sign-out"></i><span>Выход</span></a></li>
    </div>

    <main role="main" class="container">
        <div class="containerMain">
            @yield('content')
        </div>
    </main>
    <script src="/12345/jquery-3.1.1.js"></script>
    <script src="/12345/admin.js"></script>

    <footer>
        <div class="footer">

        </div>
    </footer>

</div>
</body>
</html>
