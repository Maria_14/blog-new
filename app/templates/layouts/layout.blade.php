<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="UTF-8">
    <title>
        @yield('title')
    </title>
    <link href="http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900" rel="stylesheet" type="text/css">
    <link href="/12345/main.css" rel="stylesheet">
    <link href="/12345/stylefis.css" rel="stylesheet">
</head>
<body>
<div class="wrapper">
    <div id="header-wrapper">
        <div class="container">
            <div id="logo">
                <h1><a href="/">Blog</a></h1>
            </div>
        </div>
    </div>
    <main role="main" >
        @yield('content')
    </main>
    <footer>
        Copyright © 2001-2018.
    </footer>
    <script src="/12345/jquery-3.1.1.js"></script>
    <script src="https://js.pusher.com/4.3/pusher.min.js"></script>
    @stack('scripts')
</div>
</body>
</html>