<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 30.11.18
 * Time: 19:18
 */

namespace Blog\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if (!auth()->check()) {
            return 'Error Authenticate';
        }
        return $next($request);
    }
}