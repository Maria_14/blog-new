<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 30.11.18
 * Time: 19:19
 */

namespace Blog\Middleware;

use Closure;
class StartSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        return $next($request);
    }
}