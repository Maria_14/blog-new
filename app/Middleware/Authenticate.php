<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 30.11.18
 * Time: 19:17
 */

namespace Blog\Middleware;

use Closure;


class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {


        if (!auth()->check()) {
            return redirect('login');
        }
        return $next($request);
    }
}