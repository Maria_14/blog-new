<?php
use Illuminate\Filesystem\Filesystem;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\View\Engines\CompilerEngine;
use Illuminate\View\Engines\EngineResolver;
use Illuminate\View\Engines\PhpEngine;
use Illuminate\View\Factory;
use Illuminate\View\FileViewFinder;
use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Routing\Router;
use Illuminate\Routing\UrlGenerator;
use Blog\Library\Auth;
use Blog\Library\Upload;
use Illuminate\Pagination\Paginator;




if (! function_exists('view')) {

    function view($view = null, $data = [], $mergeData = [])
    {


        $pathsToTemplates = [__ROOT__.'/templates'];
        $pathToCompiledTemplates = '/project/cache/compiled';

        $filesystem = new Filesystem;
        $eventDispatcher = new Dispatcher(new Container);

        $viewResolver = new EngineResolver;
        $bladeCompiler = new BladeCompiler($filesystem, $pathToCompiledTemplates);
        $viewResolver->register('blade', function () use ($bladeCompiler) {
            return new CompilerEngine($bladeCompiler);
        });
        $viewResolver->register('php', function () {
            return new PhpEngine;
        });
     ;
        $viewFinder = new FileViewFinder($filesystem, $pathsToTemplates);

        $viewFactory =  new Factory($viewResolver, $viewFinder, $eventDispatcher);


        Paginator::viewFactoryResolver(function () use ($viewFactory) {
            return $viewFactory;
        });

        Paginator::currentPathResolver(function () {
            return isset($_SERVER['REQUEST_URI']) ? strtok($_SERVER['REQUEST_URI'], '?') : '/';
        });

        Paginator::currentPageResolver(function ($pageName = 'page') {
            $page = isset($_REQUEST[$pageName]) ? $_REQUEST[$pageName] : 1;
            return $page;
        });

        return $viewFactory->make($view, $data, $mergeData)->render();
    }
}
if (! function_exists('abort')) {

    function abort($code, $statusCode="", $message = '', array $headers = [])
    {
        return view('404');
    }
}
if (! function_exists('redirect')) {

    function redirect($to = null, $status = 302, $headers = [], $secure = null)
    {

        $container = new Container;
        $request = Request::capture();
        $container->instance('Illuminate\Http\Request', $request);
        $events = new Dispatcher($container);

        $router = new Router($events, $container);

        $redirect = new Redirector(new UrlGenerator($router->getRoutes(), $request));

        if (is_null($to)) {
            return $redirect->back();
        }

        return $redirect->to($to, $status, $headers, $secure);
    }
}
if (! function_exists('auth')) {

    function auth()
    {

        $container = new Container;
        $auth = new Auth();
        $container->instance('auth', $auth);

        return $auth;
    }
}

if (! function_exists('upload')) {

    function upload()
    {

        $container = new Container;
        $upload = new Upload();
        $container->instance('upload', $upload);

        return $upload;
    }
}

